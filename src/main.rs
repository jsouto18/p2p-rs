extern crate futures;
extern crate hyper;


use futures::future;
use hyper::rt::{Future, Stream};
use hyper::service::service_fn;
use hyper::{Body, Method, Request, Response, Server, StatusCode};
use serde::{Deserialize, Serialize};
use serde_json;
use std::collections::HashSet;
use std::sync::{Arc, Mutex};

type BoxFut = Box<Future<Item = Response<Body>, Error = hyper::Error> + Send>;

fn echo(req: Request<Body>, state: &Arc<Mutex<SharedState>>) -> BoxFut {
	let mut response = Response::new(Body::empty());

	match (req.method(), req.uri().path()) {
		(&Method::GET, "/peers") => {
			let locked_state = state.lock().unwrap();
			*response.body_mut() =
				Body::from(serde_json::to_string(&locked_state.get_peer_list()).unwrap());
		}

		(&Method::POST, "/peer") => {
			let cloned_state = Arc::clone(state);
			let reserved = req.into_body().concat2().map(move |chunk| {
				let mut locked_state = cloned_state.lock().unwrap();
				if let Ok(peer) = serde_json::from_slice::<Peer>(&chunk) {
					locked_state.push_peer(peer);
				};
				Response::new(Body::empty())
			});
			return Box::new(reserved);
		}

		// The 404 Not Found route...
		_ => {
			*response.status_mut() = StatusCode::NOT_FOUND;
		}
	};

	Box::new(future::ok(response))
}

#[derive(Serialize, Deserialize, Eq, PartialEq, Hash, Debug)]
struct Peer {
	host: String,
	port: u32,
}

type PeerList = HashSet<Peer>;

#[derive(Debug)]
struct SharedState {
	peer_list: PeerList,
}

impl SharedState {
	fn new() -> Self {
		SharedState {
			peer_list: HashSet::new(),
		}
	}

	fn get_peer_list(&self) -> &PeerList {
		&self.peer_list
	}

	fn push_peer(&mut self, peer: Peer) {
		self.peer_list.insert(peer);
	}
}

fn main() {
	let addr = ([127, 0, 0, 1], 4000).into();

	let state = SharedState::new();
	let state_arc_ref = Arc::new(Mutex::new(state));
	let server = Server::bind(&addr)
		.serve(move || {
			let inner = Arc::clone(&state_arc_ref);
			service_fn(move |req| echo(req, &inner))
		})
		.map_err(|e| eprintln!("server error: {}", e));

	println!("Listening on http://{}", addr);
	hyper::rt::run(server);
}